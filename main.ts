import { walk, ensureDir, ensureFile } from "https://deno.land/std@0.97.0/fs/mod.ts";
//import { Application } from "https://deno.land/x/oak@v7.5.0/mod.ts";

//const port = 2001;

console.log("Welcome to the hotkey translator!");

// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, Record<string, pluginFunction>>;
type profileFunction = (keySet: Set<string>, plugins: pluginMap)=>void;

/*const app = new Application();
app.use(async (context, next) => {
  console.log(`${context.request.method} ${context.request.url.pathname}`);
  await next();
});*/

const plugins: pluginMap = {};
async function loadPlugins() {
    await ensureDir("./plugins")
    for await (const entry of walk("./plugins")){
        if(entry.isDirectory){
            continue;
        }
        const imp = await import("./" + entry.path)
        const name = entry.name.substr(0, entry.name.lastIndexOf('.')) || entry.name;
        if(typeof imp.load === "function"){
            plugins[name] = imp.load();
        }
    }
}

const profiles: Map<string, profileFunction> = new Map();
let profile: profileFunction = () => {null};
let profileName = "undefined";

plugins["profiles"] = {
    "list": () => {
        return Array.from( profiles.keys() );
    },
    "switchTo": (nextProfileName: string) => {
        const p = profiles.get(nextProfileName)
        if(p){
            profile = p;
            profileName = nextProfileName;
            console.log(`Switched to profile ${nextProfileName}`);
            return true;
        }
        console.warn(`No profile named ${nextProfileName}`);
        return false;
    }
}

async function loadProfiles() {
    await ensureDir("./profiles")
    for await (const entry of walk("./profiles")){
        if(entry.isDirectory){
            continue;
        }
        const imp = await import("./" + entry.path)
        const name = entry.name.substr(0, entry.name.lastIndexOf('.')) || entry.name;
        if(typeof imp.init == "function"){
            await imp.init(plugins);
        }
        if(typeof imp.handle === "function"){
            profiles.set(name, imp.handle);
            if(imp.takeProfile){
                profile = imp.handle
                profileName = name;
            }
        }
    }
}

console.log("loading translator key...");
await ensureFile("./translator.json")
const keyTranslations = JSON.parse(await Deno.readTextFile("./translator.json"))
function translate(keyCode: string): string {
    return keyTranslations[keyCode] ?? keyCode;
}

let last: [string, number] = ["", 0];

function handleKeypress(keyRaw: string) {

    if(keyRaw == ""){
        return;
    }
    const keyCodes = keyRaw.split(" ");
    const keyNames = keyCodes.map(translate);
    if(last[0] == keyRaw){
        if(Date.now() - last[1] <= 5){
            return;
        }
    }
    const keySet = new Set(keyNames);
    profile(keySet, plugins);
    last = [keyRaw, Date.now()];
}

console.log("loading plugins...");
await loadPlugins();
console.log("loading init script...");
{
    const imp = await import("./init.ts");
    await imp.init(plugins);
}
console.log("loading user profiles...");
await loadProfiles();
console.log(`User data loaded. First profile is ${profileName}`);
/*const server = app.listen({ port });
console.log(`Web server started at  http://127.0.0.1:${port}/`);*/
await ensureFile("./../pressed.txt")
const watcher = Deno.watchFs("./../pressed.txt");
console.log(`Now Watching "./../pressed.txt"`);
for await (const event of watcher) {
    if(event.kind != "modify"){
        continue;
    }
    const keyRaw = await Deno.readTextFile("./../pressed.txt");
	const keyStripped = keyRaw.substring(1)
    await handleKeypress(keyStripped);
}
/*await server;*/