import type {vMix} from "./../plugins/vMix.ts";
import type {PTZ} from "./../plugins/ptz.ts";
import { delay } from "https://deno.land/std@0.103.0/async/mod.ts"
// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, Record<string, pluginFunction>>;
type profileFunction = (keySet: Set<string>, plugins: pluginMap)=>void;

export const takeProfile = false

let mix: vMix | null = null
const cameras: Record<string, PTZ> = {};

export const init = (plugins: pluginMap) => {
    mix = plugins.vMix.get([0]);
    cameras.L0 = plugins.ptz.get([0]);
    cameras.R0 = plugins.ptz.get([1]);
}

const binds = new Map<string, (plugins: pluginMap, kn: Set<string>)=>void>();

export const handle: profileFunction = (kn: Set<string>, plugins: pluginMap) => {
    // Join the array's with H since no upper keys can show up.
    const bound = binds.get(Array.from(kn).join("H"));
    if(!bound){
        console.log(`unset key bind |${Array.from(kn).join(' H ')}| (${kn.size} items) in profile |test|`);
    } else {
        console.debug(bound.name)
        bound(plugins, kn);
    }
}

/**
 *  Most profile code goes down here:
 */

function HelloWorld(plugins: pluginMap) {
    plugins.test.say("Hello World!");
}
binds.set(["1"].join("H"), HelloWorld);

function Test2(plugins: pluginMap) {
    plugins.test.say("This is a test statement.");
}
binds.set(["9"].join("H"), Test2);

function switchToBroken(plugins: pluginMap) {
    plugins.profiles.switchTo("Test")
}
binds.set(["2"].join("H"), switchToBroken);

function switchToWorking(plugins: pluginMap) {
    plugins.profiles.switchTo("default")
}
binds.set(["3"].join("H"), switchToWorking);

function fadeToBlack() {
    mix?.FadeToBlack();
}
binds.set(["4"].join("H"), fadeToBlack);

async function testA() {
    console.log("Zoom")
    const {Z} = await cameras.L0.Zoom.PosInq();
    console.log(Z.toString(16));
}
binds.set(["grave_accent", "1"].join("H"), testA);
function testB() {
    console.log("Focus")
    cameras.L0.Focus.PosInq();
}
binds.set(["grave_accent", "2"].join("H"), testB);
async function testC() {
    console.log("Tilt")
    const {Y, Z} = await cameras.L0.Tilt.PosInq()
    console.log("Is At", {Y:"0x" + Y.toString(16), Z: "0x" + Z.toString(16)});
    await delay(1000)
    cameras.L0.Tilt.Absolute(0x18, 0x17, Y, Z);
}
binds.set(["grave_accent", "3"].join("H"), testC);
async function testD() {
    await cameras.L0.Zoom.Direct(0)
    await cameras.L0.Zoom.Direct(1)
}
binds.set(["grave_accent", "4"].join("H"), testD);
/*async function testE() {
    console.log("Off");
    cameras.L0.Power.Off();
    cameras.R0.Power.Off();
    await delay(1000)
    console.log("On")
    cameras.L0.Power.On();
    cameras.R0.Power.On();
}*/
async function testE() {
    console.log("Limit Low");
    cameras.L0.Zoom.Direct(0);
    // Move to the low limit.
    await cameras.L0.Tilt.Absolute(0x18, 0x17, (-0x2200 + 1), (-0x0400 + 1)); // must be one below limit
    const a = await cameras.L0.Tilt.PosInq();
    await cameras.L0.Tilt.Absolute(0x18, 0x17, a.Y, a.Z);
    const b = await cameras.L0.Tilt.PosInq();
    if(a.Y == b.Y && a.Z == b.Z){
        console.log("Physical Min is", {y: a.Y, z: a.Z});
    }
    console.log("When developing this was", { y: -2447, z: -1023 })
}
binds.set(["grave_accent", "5"].join("H"), testE);
function testF() {
    console.log("Limit Mid");
    cameras.L0.Zoom.Direct(0);
    // Move to the home.
    cameras.L0.Tilt.Absolute(0x18, 0x17, 0x0000, 0x0000);
}
binds.set(["grave_accent", "6"].join("H"), testF);
async function testG() {
    console.log("Limit High");
    await cameras.L0.Zoom.Direct(0);
    // Move to the high limit.
    await cameras.L0.Tilt.Absolute(0x18, 0x17, 0x2200, 0x2200);
    const a = await cameras.L0.Tilt.PosInq();
    await cameras.L0.Tilt.Absolute(0x18, 0x17, a.Y, a.Z);
    const b = await cameras.L0.Tilt.PosInq();
    if(a.Y == b.Y && a.Z == b.Z){
        console.log("Physical Max is", {y: a.Y, z: a.Z});
    }
    console.log("When developing this was", { y: 2448, z: 432 })
}
binds.set(["grave_accent", "7"].join("H"), testG);

function recall1() {
    cameras.L0.Memory.Recall(1);
    cameras.R0.Memory.Recall(1);
}
binds.set(["numpad_1"].join("H"), recall1);
function recall2() {
    cameras.L0.Memory.Recall(2);
    cameras.R0.Memory.Recall(2);
}
binds.set(["numpad_2"].join("H"), recall2);
function recall3() {
    cameras.L0.Memory.Recall(3);
    cameras.R0.Memory.Recall(3);
}
binds.set(["numpad_3"].join("H"), recall3);
function recall4() {
    cameras.L0.Memory.Recall(4);
    cameras.R0.Memory.Recall(4);
}
binds.set(["numpad_4"].join("H"), recall4);
function recall5() {
    cameras.L0.Memory.Recall(5);
    cameras.R0.Memory.Recall(5);
}
binds.set(["numpad_5"].join("H"), recall5);
function recall6() {
    cameras.L0.Memory.Recall(6);
    cameras.R0.Memory.Recall(6);
}
binds.set(["numpad_6"].join("H"), recall6);
function recall7() {
    cameras.L0.Memory.Recall(7);
    cameras.R0.Memory.Recall(7);
}
binds.set(["numpad_7"].join("H"), recall7);
function recall8() {
    cameras.L0.Memory.Recall(8);
    cameras.R0.Memory.Recall(8);
}
binds.set(["numpad_8"].join("H"), recall8);
function recall9() {
    cameras.L0.Memory.Recall(9);
    cameras.R0.Memory.Recall(9);
}
binds.set(["numpad_9"].join("H"), recall9);

/**
 * For powering up or down the cameras.
 * 
 * Doesn't work right now.
 */
/*
function PowerUp() {
    cameras.L0.Power.On();
    cameras.R0.Power.On();
}
function PowerDown() {
    cameras.L0.Power.On();
    cameras.R0.Power.On();
}
// NumLock Off + 7/Home
binds.set(["home"].join("H"), PowerUp);
// NumLock Off + 1/End
binds.set(["end"].join("H"), PowerDown);
*/