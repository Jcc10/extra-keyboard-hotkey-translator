// deno-lint-ignore-file no-unused-labels no-inner-declarations
import type {vMix} from "./../plugins/vMix.ts";
import type {PTZ} from "./../plugins/ptz.ts";
import { delay } from "https://deno.land/std@0.103.0/async/mod.ts"
import { names, recall } from "../configs/mode.ts"
// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, Record<string, pluginFunction>>;
type profileFunction = (keySet: Set<string>, plugins: pluginMap)=>void;

export const takeProfile = true

let mix: vMix | null = null
const cameras: Record<string, PTZ> = {};

export const init = (plugins: pluginMap) => {
    mix = plugins.vMix.get([0]);
    cameras.C0 = plugins.ptz.get([0]);
    cameras.R0 = plugins.ptz.get([1]);
}

const binds = new Map<string, (plugins: pluginMap, kn: Set<string>)=>void>();

export const handle: profileFunction = (kn: Set<string>, plugins: pluginMap) => {
    // Join the array's with H since no upper keys can show up.
    const bound = binds.get(Array.from(kn).join("H"));
    if(!bound){
        console.log(`unset key bind |${Array.from(kn).join(' H ')}| (${kn.size} items) in profile |default|`);
    } else {
        console.debug(bound.name)
        bound(plugins, kn);
    }
}

/**
 *  Most profile code goes down here:
 */

/**
 * Profile Switching
 */
profiles:{

    // Switch to test profile.
    function profileSwapDown(plugins: pluginMap) {
        plugins.profiles.switchTo("test");
    }
    binds.set(["page_down"].join("H"), profileSwapDown);

}

/**
 * View Selection Code
 */
let activeSelection = 0;
let viewSelection = 0;
let lastActive = 0;

const allPTZ = ["C0", "R0"];

const PTZMap = [
    [],
    ["C0"],
    ["R0"],
    [],
    [],
    ["C0", "R0"],
    ["C0", "R0"],
    ["C0", "R0"],
    ["C0", "R0"],
    [],
    [],
    [],
    [],
];

function notHotPTZ() {
    const notLiveSet = new Set(allPTZ);
    for(const cam of PTZMap[activeSelection]){
        notLiveSet.delete(cam);
    }
    return [...notLiveSet]
}

async function localCall(__: pluginMap, kn: Set<string>) {
    const prom = [];
    let preset = "test";
    switch([...kn.values()][0]){
        case "a":
            preset = "test";
            break;
        default:
            preset = "test";
            break;
    }
    for(const cam of notHotPTZ()){
        const pos = recall(preset, cam);
        prom.push(cameras[cam].Tilt.Absolute(0x18, 0x14, pos.Y, pos.Z));
        prom.push(cameras[cam].Zoom.Direct(pos.zoom));
        if(pos.af){
            prom.push(cameras[cam].Focus.Auto());
        }else{
            console.warn("NOPE!")
        }
    }
    await Promise.all(prom);
}
async function localCallSet(plugin: pluginMap, kn: Set<string>) {
    plugin;
    await kn;
    console.log([...kn.values()][1]);
}
for(let x = 97; x <= 122; x++) {
    const char = String.fromCharCode(x);
    binds.set([char].join("H"), localCall);
    binds.set(["tab",char].join("H"), localCallSet);
}


async function viewSelector(__: pluginMap, kn: Set<string>) {
    const key = [...kn].join("");
    for(const camName of PTZMap[viewSelection]){
        if(PTZMap[activeSelection].includes(camName))
            continue;
        if(cameras[camName]?.isBusy)
            continue;
        await cameras[camName]?.TallyLight.Set(0);
    }
    switch(key){
        case "escape":
            viewSelection = 0;
            break;
        case "f1":
            viewSelection = 1;
            break;
        case "f2":
            viewSelection = 2;
            break;
        case "f3":
            viewSelection = 3;
            break;
        case "f4":
            viewSelection = 4;
            break;
        case "f5":
            viewSelection = 5;
            break;
        case "f6":
            viewSelection = 6;
            break;
        case "f7":
            viewSelection = 7;
            break;
        case "f8":
            viewSelection = 8;
            break;
        case "f9":
            viewSelection = 9;
            break;
        case "f10":
            viewSelection = 10;
            break;
        case "f11":
            viewSelection = 11;
            break;
        case "f12":
            viewSelection = 12;
            break;
        default:
            viewSelection = 0;
    }
    if(mix){
        mix.Preview(viewSelection + 1);
    }
    for(const camName of PTZMap[viewSelection]){
        if(PTZMap[activeSelection].includes(camName))
            continue;
        if(cameras[camName]?.isBusy)
            continue;
        await cameras[camName]?.TallyLight.Set(1);
    }
    console.log(`View ${viewSelection}`);
}
binds.set(["escape"].join("H"), viewSelector);
for(let i = 1; i <= 12; i++){
    binds.set([`f${i}`].join("H"), viewSelector);
}

/**
 * set preview to last active
 */
async function previewLast() {
    viewSelection = lastActive;
    if(mix){
        mix.Preview(viewSelection + 1);
    }
    for(const camName of PTZMap[viewSelection]){
        if(PTZMap[activeSelection].includes(camName))
            continue;
        if(cameras[camName]?.isBusy)
            continue;
        await cameras[camName]?.TallyLight.Set(1);
    }
    console.log(`View ${viewSelection}`);
}
binds.set(["add"].join("H"), previewLast);


/**
 * Cut/Fade Options
 */
async function numSwitch(__: pluginMap, kn: Set<string>) {
    if(mix === null){
        console.log("vMix isn't on!");
        return;
    }
    if(activeSelection == viewSelection){
        return;
    }
    for(const camName of PTZMap[activeSelection]){
        if(PTZMap[viewSelection].includes(camName))
            continue;
        if(cameras[camName]?.isBusy)
            continue;
        await cameras[camName]?.TallyLight.Set(0);
    }
    lastActive = activeSelection;
    activeSelection = viewSelection;
    const key = [...kn].join("");
    console.log(key);
    switch(key){
        case "numpad_1":
            mix.Transition.Fade(viewSelection + 1, 250);
            break;
        case "numpad_2":
            mix.Transition.Fade(viewSelection + 1, 500);
            break;
        case "numpad_3":
            mix.Transition.Fade(viewSelection + 1, 750);
            break;
        case "numpad_4":
            mix.Transition.Fade(viewSelection + 1, 1000);
            break;
        case "numpad_5":
            mix.Transition.Fade(viewSelection + 1, 1500);
            break;
        case "numpad_6":
            mix.Transition.Fade(viewSelection + 1, 2000);
            break;
        case "numpad_7":
            mix.Transition.CrossZoom(viewSelection + 1, 250);
            break;
        case "numpad_8":
            mix.Transition.CrossZoom(viewSelection + 1, 500);
            break;
        case "numpad_9":
            mix.Transition.CrossZoom(viewSelection + 1, 750);
            break;
        case "insert":
            null;
            /* falls through */
        case "numpad_0":
            mix.Transition.Cut(viewSelection + 1);
            break;
        default:
            null;
    }
    for(const camName of PTZMap[activeSelection]){
        if(cameras[camName]?.isBusy)
            continue;
        cameras[camName]?.TallyLight.Set(2);
        await delay(10);
    }
    console.log(`Active is now ${viewSelection}`);
}
binds.set(["insert"].join("H")  , numSwitch);
for(let i = 0; i <= 9; i++){
    binds.set([`numpad_${i}`].join("H"), numSwitch);
}

/**
 * Title Selection Code.
 */
title:{
    let isIn = false;


    function titleSwitch(__: pluginMap, kn: Set<string>) {
        if(mix === null){
            console.log("vMix isn't on!");
            return;
        }
        if(isIn){
            return;
        }
        const key = [...kn].join("");
        const {name, title} = names(key);
        mix.Title.SetText(14, "Headline", name);
        mix.Title.SetText(14, "Description", title);
    }
    for(let i = 0; i <= 9; i++){
        binds.set([`${i}`].join("H"), titleSwitch);
    }


    function inOut() {
        if(mix === null){
            console.log("vMix isn't on!");
            return;
        }
        if(isIn){
            mix.Title.Out(14);
        } else {
            mix.Title.In(14);
        }
        isIn = !isIn;
    }
    binds.set(["grave_accent"].join("H"), inOut);


    async function autoTitle() {
        if(mix === null){
            console.log("vMix isn't on!");
            return;
        }
        mix.Title.In(14);
        isIn = true;
        await delay(5000);
        mix.Title.Out(14);
        isIn = false;
    }
    binds.set(["scroll", "44"].join("H"), autoTitle);
}

/**
 * Auto Switch Code
 */

autoSwitch:{
    let isAutomated = false;
    function offset(Original: Date,
        Hours = 0, Minutes = 0, Seconds = 0, MS = 0): Date {
        return new Date( Original.getTime() + 
        (
            (
                ((
                    ((
                        Hours * 60
                    ) + Minutes * 60
                ) + Seconds) * 1000
            ) + MS
        )
        ));
    }

    function getTimeTo() {
        const startd = new Date();
        //               HH + HH   MM  SS  MS
        startd.setHours((10 +  0),  0,  0,  0);
        const streamo = offset(startd, 0, -7, -30, 0);
        const recordo = offset(startd, 0, -3, 0, 0);
        const specialo = offset(startd, 0, -1, -58, 0);
        const start = new Date(startd.getTime() - new Date().getTime()).getTime();
        const stream = new Date(streamo.getTime() - new Date().getTime()).getTime();
        const record = new Date(recordo.getTime() - new Date().getTime()).getTime();
        const special = new Date(specialo.getTime() - new Date().getTime()).getTime();
        return {
            start,
            stream,
            record,
            special,
        }
    }
    async function autoStart() {
        if(isAutomated){
            return;
        }
        isAutomated = true;
        if(mix === null){
            console.log("vMix isn't on!");
            return;
        }
        mix.Transition.Cut(1);
        mix.Title.In(19);
        let gtt = getTimeTo()
        //TODO: GENERALISE FOR DIFFRENT TIME OFFSETS.
        while(gtt["stream"] > 0){
            gtt = getTimeTo()
            await delay((gtt["stream"] / 2) + 1);
        }
        await mix.Streams.start();
        console.log("streaming...")
        while(gtt["record"] > 0){
            gtt = getTimeTo()
            await delay((gtt["record"] / 2) + 1);
        }
        await mix.Recording.start();
        console.log("recording...")
        while(gtt["special"] > 0){
            gtt = getTimeTo()
            await delay((gtt["special"] / 2) + 1);
        }
        // REMOVE THIS.
        await mix.Transition.Fade(11, 500);
        while(gtt["start"] > 0){
            gtt = getTimeTo()
            await delay((gtt["start"] / 2) + 1);
        }
        mix.Title.Out(19);
        mix.Transition.CrossZoom(17, 500);
        await delay(500);
        isAutomated = false;
    }
    async function autoEnd() {
        if(isAutomated){
            return;
        }
        isAutomated = true;
        if(mix === null){
            console.log("vMix isn't on!");
            return;
        }
        mix.Transition.Fade(18, 1000);
        await delay(5500);
        mix.Transition.CrossZoom(1, 500);
        await delay(10000);
        mix.FadeToBlack();
        await delay(500);
        mix.Recording.stop();
        isAutomated = false;
    }

    binds.set(["home"].join("5"), autoStart);
    binds.set(["end"].join("5"), autoEnd);
}

/**
 * Calibration Tools
 */
calibration:{
    async function stateInq() {
        for(const camName of PTZMap[viewSelection]){
            let pos: {zoom: number, focus: number, af: boolean,
            X: number, Y: number, Z: number} =
            {zoom: 0, focus: 0, af: true,
                X: 0, Y: 0, Z: 0};
            const res1 = await cameras[camName]?.Tilt.PosInq();
            if(!res1){
                console.log(`${camName} is not PTZ`);
            } else {
                pos = {...pos, ...res1};
            }
            const res2 = await cameras[camName]?.Zoom.PosInq();
            if(!res2){
                console.log(`${camName} is not PTZ`);
            } else {
                pos = {...pos, zoom: res2.Z};
            }
        }
    }
    binds.set(["12"].join("H"), stateInq);

    async function shutdown(__: pluginMap, kn: Set<string>) {
        if(mix === null){
            console.log("vMix isn't on!");
            return;
        }
        const key = [...kn][2];
        console.log(key);
        let time = 6000;
        switch(key){
            case "numpad_1":
                time = 5
                break;
            case "numpad_2":
                time = 7.5
                break;
            case "numpad_3":
                time = 10
                break;
            case "numpad_4":
                time = 15
                break;
            case "numpad_5":
                time = 30
                break;
            case "numpad_6":
                time = 45
                break;
            case "numpad_7":
                time = 60
                break;
            case "numpad_8":
                time = 60 + 15
                break;
            case "numpad_9":
                time = 60 + 30
                break;
            default:
                null;
        }
        console.log(`"Auto shutdown in ${time * 60} seconds (${time} minutes)`)
        const cmd = ["shutdown", "/s", `/t ${time * 60}`, `/c "Finished Upload"`]
        await console.debug(cmd.join(" "))
        //await Deno.run({cmd});
    }
    for(let i = 1; i <= 9; i++){
        binds.set([`17|0`, `18|0`, `numpad_${i}`].join("H"), shutdown);
    }
}