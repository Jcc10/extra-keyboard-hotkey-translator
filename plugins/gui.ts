/**import { Webview } from "https://deno.land/x/webview/mod.ts";

import * as path from "https://deno.land/std@0.125.0/path/mod.ts";
import { readableStreamFromReader } from "https://deno.land/std@0.125.0/streams/mod.ts";

async function serveGui() {

  // Start listening on port 8080 of localhost.
  const server = Deno.listen({ port: 777 });
  console.log("GUI HTML server running on http://localhost:777/");

  for await (const conn of server) {
    handleHttp(conn);
  }

  async function handleHttp(conn: Deno.Conn) {
    const httpConn = Deno.serveHttp(conn);
    for await (const requestEvent of httpConn) {
      // Use the request pathname as filepath
      const url = new URL(requestEvent.request.url);
      const filepath = decodeURIComponent(url.pathname);

      // Try opening the file
      let file;
      try {
        file = await Deno.open("./htmlGui/" + filepath, { read: true });
        const stat = await file.stat();

        // If File instance is a directory, lookup for an index.html
        if (stat.isDirectory) {
          file.close();
          const filePath = path.join("./htmlGui/", filepath, "index.html");
          file = await Deno.open(filePath, { read: true });
        }
      } catch {
        // If the file cannot be opened, return a "404 Not Found" response
        const notFoundResponse = new Response("404 Not Found", { status: 404 });
        await requestEvent.respondWith(notFoundResponse);
        return;
      }

      // Build a readable stream so the file doesn't have to be fully loaded into
      // memory while we send it
      const readableStream = readableStreamFromReader(file);

      // Build and send the response
      const response = new Response(readableStream);
      await requestEvent.respondWith(response);
    }
  }
}

function displayGui() {

  const webview = new Webview();
  webview.navigate(`http://localhost:777/test.html`);
  const wbxw = webview.run();
  i_guess_ill_die(wbxw);
  webview.eval(`updateTitle("GUI ON!")`);
}

async function i_guess_ill_die(waiter: Promise<any>){
  await waiter;
  Deno.exit();
}

serveGui();
displayGui();
**/