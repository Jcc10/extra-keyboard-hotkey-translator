import { Client, Packet, Event } from "https://deno.land/x/tcp_socket@0.0.2/mods.ts";
import { deferred, Deferred } from "https:///deno.land/std@0.103.0/async/mod.ts";
import { delay } from "https://deno.land/std@0.103.0/async/mod.ts"

/**
 * The binary being implemented is from:
 * https://ptzoptics.com/wp-content/uploads/2020/11/PTZOptics-VISCA-over-IP-Rev-1_2-8-20.pdf
 */


// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, pluginFunction>;
let map: pluginMap;
let cameras: Map<number, PTZ>;
export function load() {
    cameras = new Map();
    map = {};
    map["new"] = ([host, id]: [string, number | undefined | null]): number => {
        const n = id ?? cameras.size;
        // deno-lint-ignore camelcase
        const new_ptz = new PTZ(host)
        cameras.set(n, new_ptz);
        return n;
    };
    map["get"] = ([id]: [number]) => {
        return cameras.get(id);
    }
    return map;
}

const fromHexString = (hexString: string) => {
    const bytes = hexString.replaceAll(" ", "").match(/.{1,2}/g);
    if(!bytes) return undefined;
    return new Uint8Array(bytes.map(byte => parseInt(byte, 16)));
}

const toHexString = (bytes: Uint8Array) => {
    return bytes.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '');
}

const padHexFromDec = (d: number): string => {
        return ('00' + d.toString(16).toUpperCase()).slice(-2);
}

const inRange = (comp: number, start: number, end: number): boolean => {
    return (comp >= start && comp <= end)
}

// deno-lint-ignore no-unused-vars
const toRange = (x: number, startMin: number, startMax: number, endMin: number, endMax: number) => {
    return (x - startMin) * (endMax - endMin) / (startMax - startMin) + endMin;
}

const enum RetPkg {
    // deno-lint-ignore camelcase
    Unknown_50,
    // deno-lint-ignore camelcase
    Unknown_60,
    // deno-lint-ignore camelcase
    Unknown_Oth,

    ACK_ACK,
    // deno-lint-ignore camelcase
    ACK_Complete,

    // deno-lint-ignore camelcase
    ERR_Syntax,
    // deno-lint-ignore camelcase
    ERR_BufferFull,
    // deno-lint-ignore camelcase
    ERR_Canceled,
    // deno-lint-ignore camelcase
    ERR_NoSocket,
    // deno-lint-ignore camelcase
    ERR_NoExe,

    // deno-lint-ignore camelcase
    INQ_ZoomPos,
    // deno-lint-ignore camelcase
    INQ_FocusMode,
    // deno-lint-ignore camelcase
    INQ_FocusPos,
    // deno-lint-ignore camelcase
    INQ_WBMode,
    // deno-lint-ignore camelcase
    INQ_RGain,
    // deno-lint-ignore camelcase
    INQ_BGain,
    // deno-lint-ignore camelcase
    INQ_ColorTemp,
    // deno-lint-ignore camelcase
    INQ_ExpMode, // Check Name
}

export class PTZ {
    readonly host:string;
    readonly port:number = 5678;
    private inqResp: Deferred<Array<number>> | null = null;
    private ackResp: Deferred<number> | null = null;
    private compResp: [Deferred<true>, Deferred<true>] = [deferred(), deferred()];
    public timeout = 500;
    socket?: Client;

    constructor(host: string) {
        this.host = host;
    }

    public get isBusy() {
        if(this.inqResp || this.ackResp)
            return true;
        return false;
    }

    private async timeoutFunctionasync() {
        await delay(this.timeout);
        return -1;
    }

    private static rawToPacketAndLeftovers(UIA: Uint8Array, previousArray: Array<number>): {Packet?: Array<number>, Leftover: Array<number>} {
        const main: Array<number> = [];
        const Leftover: Array<number> = [];

        let first = false;
        let last = false;
        
        for(const byte of previousArray){
            if(first == false && byte == 0x90){
                first = true;
            }
            if(!last){
                main.push(byte);
            } else {
                Leftover.push(byte);
            }
            if(last == false && byte == 0xFF){
                last = true;
            }
        }
        for(const byte of UIA){
            if(first == false && byte == 0x90){
                first = true;
            }
            if(!last){
                main.push(byte);
            } else {
                Leftover.push(byte);
            }
            if(last == false && byte == 0xFF){
                last = true;
            }
        }
        if(last){
            main.pop()
            main.shift();
            return {Packet: main, Leftover};
        } else {
            return {Leftover: main};
        }
    }

    private static packetParser(packet: Array<number>): {type: RetPkg, vars: Array<number>} {
        const vars: Array<number> = [];
        // ACK ACK
        if(packet[0] >= 0x41 && packet[0] < 0x50) {
            vars.push(packet[0] - 0x41)
            return {type: RetPkg.ACK_ACK, vars}
        // ACK Complete
        } else if(packet[0] >= 0x51 && packet[0] < 0x60) {
            vars.push(packet[0] - 0x51)
            return {type: RetPkg.ACK_Complete, vars}
        // ERRORS
        } else if(packet[0] >= 0x60 && packet[0] < 0x70) {
            // Syntax
            if(packet[1] == 0x02){
                return {type: RetPkg.ERR_Syntax, vars}
            // Buffer
            } else if(packet[1] == 0x03){
                return {type: RetPkg.ERR_BufferFull, vars}
            }
            // Socket Dependent
            vars.push(packet[0] - 0x61);
            // Canceled
            if(packet[1] == 0x04){
                return {type: RetPkg.ERR_Canceled, vars}
            // No Socket
            } else if(packet[1] == 0x05){
                return {type: RetPkg.ERR_NoSocket, vars}
            // No Exe
            } else if(packet[1] == 0x41){
                return {type: RetPkg.ERR_NoExe, vars}
            }
            return {type: RetPkg.Unknown_60, vars}
        // Data Returns
        } else if(packet[0] == 0x50) {
            return {type: RetPkg.Unknown_50, vars}
        }
        return {type: RetPkg.Unknown_Oth, vars}
    }

    async connect() {
        this.socket = new Client({
            hostname: this.host,
            port: this.port,
            transport: "tcp"
        });
        // Connection open
        this.socket.on(Event.connect, (_client: Client) => {
            console.log(`Connected to PTZ://${this.host}:${this.port}`)
        });
        
        let lastLeftover: Array<number> = [];

        const packetHandeler = (data: Uint8Array) => {
            const {Packet, Leftover} = PTZ.rawToPacketAndLeftovers(data, lastLeftover);
            lastLeftover = Leftover;
            if(!Packet){
                console.log("Received Partial:", toHexString(new Uint8Array(Leftover)));
                return;
            }
            const {type, vars} = PTZ.packetParser(Packet);
            if(type == RetPkg.ACK_ACK){
                this.ackResp?.resolve(vars[0]);
                this.ackResp = null;
            } else if(type == RetPkg.ACK_Complete){
                this.compResp[vars[0]]?.resolve(true);
                this.compResp[vars[0]] = deferred();
            } else if(type == RetPkg.Unknown_50){
                Packet.shift(); // Remove the 0x50 from the front.
                this.inqResp?.resolve(Packet);
                this.inqResp = null;
            } else {
                console.log(`Received ${type ?? "Unknown"}:`, toHexString(new Uint8Array(Packet)));
            }
            if(Leftover.length > 0){
                packetHandeler(new Uint8Array([])); // If we get more than one packet at a time, run through all of them.
            }
        }


        // Receive message
        this.socket.on(Event.receive, (_client: Client, data: Packet) => packetHandeler(data.toData()));
        // Connection close
        this.socket.on(Event.close, (_client: Client) => {
            this.inqResp?.reject();
            console.log(`Reconnecting to PTZ://${this.host}:${this.port}...`);
            this.connect();
        });
        
        // Handle error
        this.socket.on(Event.error, (e:unknown) => {
            console.error("Error:", e);
        });
        return await this.socket.connect();
    }

    private async sendHex(hex: string){
        const bytes = fromHexString(hex);
        if(bytes) {
            await this.safeSend(bytes);
        }
    }

    private async safeSend(data: Uint8Array, debug = false){
        if(this.socket){
            while(this.ackResp){
                await this.ackResp;
            }
            const ackResp = this.ackResp = deferred();
            const compResps = [this.compResp[0], this.compResp[1]];
            debug ? console.debug(`sending Hex ${toHexString(data)}`) : null;
            const timeout = (async () => {await delay(this.timeout); return -1})();
            await this.socket?.write(data);
            const num = await Promise.any([ackResp, timeout]);
            return compResps[num];
        } else {
			console.warn(`Not connected to PTZ://${this.host}:${this.port} yet!`);
            return true;
        }
    }

    private async safeInq(data: Uint8Array, debug = false){
        if(this.socket){
            while(this.inqResp){
                await this.inqResp;
            }
            const inqResp = this.inqResp = deferred();
            debug ? console.debug(`sending Inq ${toHexString(data)}`) : null;
            await this.socket?.write(data);
            return inqResp;
        } else {
			console.warn(`Not connected to PTZ://${this.host}:${this.port} yet!`);
            return Array.from([0]);
        }
    }

    private static PowerPayloads = {
        on: new Uint8Array([0x81, 0x01, 0x04, 0x00, 0x02, 0xFF]),
        off: new Uint8Array([0x81, 0x01, 0x04, 0x00, 0x03, 0xFF]),
    }

    get Power() {
        const inter = {
            On: async () => {
                await this.safeSend(PTZ.PowerPayloads.on);
            },
            Off: async () => {
                await this.safeSend(PTZ.PowerPayloads.off);
            }
        }
        return inter;
    }

    private static ZoomPayloads = {
        stop: new Uint8Array([0x81, 0x01, 0x04, 0x07, 0x00, 0xFF]),
        posInq: new Uint8Array([0x81, 0x09, 0x04, 0x47, 0xFF]),
    }

    get Zoom() {
        const inter = {
            Stop: async () => {
                await this.safeSend(PTZ.ZoomPayloads.stop);
            },
            Tele: async (p?: number) => {
                if(p){
                    const n = p
                    if(n < 0 || n > 7){
                        console.warn("Invalid Zoom Tele p.")
                        return
                    }
                    await this.sendHex(`81 01 04 07 2${padHexFromDec(n)} FF`)
                } else {
                    await this.sendHex(`81 01 04 07 02 FF`)
                }
            },
            Wide: async (p?: number) => {
                if(p){
                    const n = p
                    if(n < 0 || n > 7){
                        console.warn("Invalid Zoom Wide p.")
                        return
                    }
                    await this.sendHex(`81 01 04 07 3${padHexFromDec(n)} FF`)
                } else {
                    await this.sendHex(`81 01 04 07 03 FF`)
                }
            },
            DirectRaw: async (z: number) => {
                if(!inRange(z, 0x0000, 0x4000)){
                    console.warn("Invalid Zoom Z")
                    return
                }
                const [p, q, r, s]: [number, number, number, number] = [
                    (z & 0xF0_00) >>> (4 * 3),
                    (z & 0x0F_00) >>> (4 * 2),
                    (z & 0x00_F0) >>> (4 * 1),
                    (z & 0x00_0F) >>> (4 * 0),
                ]
                return await this.safeSend(new Uint8Array([0x81, 0x01, 0x04, 0x47, p, q, r, s, 0xFF]))
            },
            Direct: async (z: number) => {
                if( z > 1)
                    z = 1;
                if(z < 0)
                    z = 0;
                const az = z * 0x4000
                return await this.Zoom.DirectRaw(az);
            },
            PosInq: async (): Promise<{Z: number}> => {
                const rsp = await this.safeInq(PTZ.ZoomPayloads.posInq, true);
                const Z: number = (
                    (rsp[3] << (4 * 0)) +
                    (rsp[2] << (4 * 1)) +
                    (rsp[1] << (4 * 2)) +
                    (rsp[0] << (4 * 3))
                )
                return {Z}
            }
        }
        return inter;
    }

    get Focus() {
        const inter = {
            Stop: async () => {
                await this.sendHex(`81 01 04 08 00 FF`)
            },
            Far: async (p?: number) => {
                if(p){
                    const n = p
                    if(n < 0 || n > 7){
                        console.warn("Invalid Zoom Wide p.")
                        return
                    }
                    await this.sendHex(`81 01 04 08 2${padHexFromDec(n)} FF`)
                } else {
                    await this.sendHex(`81 01 04 08 02 FF`)
                }
            },
            Near: async (p?: number) => {
                if(p){
                    const n = p
                    if(n < 0 || n > 7){
                        console.warn("Invalid Zoom Wide p.")
                        return
                    }
                    await this.sendHex(`81 01 04 08 3${padHexFromDec(n)} FF`)
                } else {
                    await this.sendHex(`81 01 04 08 03 FF`)
                }
            },
            DirectRaw: async (p: number, q: number, r: number, s: number) => {
                if(p < 0x00 || p > 0x0f){
                    console.warn("Invalid Zoom P")
                    return
                }
                if(q < 0x00 || q > 0x0f){
                    console.warn("Invalid Zoom Q")
                    return
                }
                if(r < 0x00 || r > 0x0f){
                    console.warn("Invalid Zoom R")
                    return
                }
                if(s < 0x00 || s > 0x0f){
                    console.warn("Invalid Zoom S")
                    return
                }
                await this.sendHex(`81 01 04 48 ${padHexFromDec(p)} ${padHexFromDec(q)} ${padHexFromDec(r)} ${padHexFromDec(s)} FF`)
            },
            Auto: async (): Promise<true | undefined> => {
                await this.sendHex(`81 01 04 38 02 FF`);
                return;
            },
            Manual: async (): Promise<true | undefined> => {
                await this.sendHex(`81 01 04 38 03 FF`)
                return
            },
            ToggleAutoManual: async () => {
                await this.sendHex(`81 01 04 38 10 FF`)
            },
            Lock: async () => {
                await this.sendHex(`81 0a 04 68 02 FF`)
            },
            Unlock: async () => {
                await this.sendHex(`81 0a 04 68 03 FF`)
            },
            //CAM_AFZone
            //CAM_AFCalibration
            AFModeInq: async (): Promise<"Auto" | "Manual" | "Unknown"> => {
                await console.warn("Stub");
                return "Unknown";
            },
            PosInq: async (): Promise<{p: number, q: number, r: number, s:number}> => {
                const rsp = await this.safeInq(new Uint8Array([0x81, 0x09, 0x06, 0x12, 0xFF]), true);
                console.debug(toHexString(new Uint8Array(rsp)));
                return {p: -1, q: -1, r: -1, s: -1}
            }
        }
        return inter;
    }

    get WB() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
            //CAM_AWBSensitivity
        }
        return inter;
    }

    get Gain() {
        //CAM_RGain
        const Red = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        //CAM_BGain
        const Blue = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        const inter = {
            Red,
            Blue,
            //CAM_Gain
            Stub: () => {
                console.warn("Stub");
            }
            //CAM_ColorGain
        }
        return inter;
    }

    get ColorTemp() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Exposure() {
        // CAM_AE
        const Mode = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        // CAM_ExpComp
        const Comp = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        const inter = {
            Mode, Comp
        }
        return inter;
    }

    get Iris() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Shutter() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Bright() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Backlight() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Flicker() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Aperture() {
        const Mode = {
            Auto: async () => {
                await this.sendHex(`81 01 04 05 02 FF`)
            },
            Manual: async () => {
                await this.sendHex(`81 01 04 05 03 FF`)
            },
        }
        const inter = {
            Mode,
            Reset: async () => {
                await this.sendHex(`81 01 04 02 00 FF`)
            },
            Up: async () => {
                await this.sendHex(`81 01 04 02 02 FF`)
            },
            Down: async () => {
                await this.sendHex(`81 01 04 02 03 FF`)
            },
            Direct: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Effects() {
        const inter = {
            Off: async () => {
                await this.sendHex(`81 01 04 63 00 FF`)
            },
            BnW: async () => {
                await this.sendHex(`81 01 04 63 04 FF`)
            },
        }
        return inter;
    }

    get Memory() {
        const inter = {
            Reset: async (memory: number) => {
                const n = memory
                if(n < 0 || n > 127){
                    console.warn("Invalid Memory Address")
                    return
                }
                await this.safeSend(new Uint8Array([0x81, 0x01, 0x04, 0x3F, 0x00, n, 0xFF]))
            },
            Set: async (memory: number) => {
                const n = memory
                if(n < 0 || n > 127){
                    console.warn("Invalid Memory Address")
                    return
                }
                await this.safeSend(new Uint8Array([0x81, 0x01, 0x04, 0x3F, 0x01, n, 0xFF]))
            },
            Recall: async (memory: number) => {
                const n = memory
                if(n < 0 || n > 127){
                    console.warn("Invalid Memory Address")
                    return
                }
                await this.safeSend(new Uint8Array([0x81, 0x01, 0x04, 0x3F, 0x02, n, 0xFF]))
            },
            Speed: async (speed: number) => {
                const n = speed;
                // I spent 5 minutes trying to figure out how to test this
                // before I realized I can just enter the Hex value.
                if(n < 0x01 || n > 0x18){
                    console.warn("Invalid Speed Value")
                    return
                }
                await this.safeSend(new Uint8Array([0x81, 0x01, 0x06, 0x01, n, 0xFF]))
            }
        };
        return inter
    }

    get Flip() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Tilt() {
        const Limit = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        const inter = {
            Limit,
            Stub: () => {
                console.warn("Stub");
            },
            Absolute: async (v: number, w: number, y: number, z: number) => {
                if(!inRange(v, 0x01, 0x18)){
                    console.warn("Invalid Tilt V");
                    return
                }
                if(!inRange(w, 0x01, 0x17)){
                    console.warn("Invalid Tilt W")
                    return
                }
                if(inRange(y, -0x2200, -0x0001)){
                    y = y + 0xFFFF
                }
                if(!inRange(y, 0x0000, 0x2200) && !inRange(y, 0xDE00, 0xFFFF)){
                    console.warn("Invalid Tilt Y")
                    return
                }
                if(inRange(z, -0x0400, -0x0001)){
                    z = z + 0xFFFF
                }
                if(!inRange(z, 0x0000, 0x2200) && !inRange(z, 0xFC00, 0xFFFF)){
                    console.warn("Invalid Tilt Z")
                    return
                }
                //Smallest Bytes first? Lol Ok.
                const Y: [number, number, number, number] = [
                    (y & 0x00_0F) >>> (4 * 0),
                    (y & 0x00_F0) >>> (4 * 1),
                    (y & 0x0F_00) >>> (4 * 2),
                    (y & 0xF0_00) >>> (4 * 3),
                ]
                const Z: [number, number, number, number] = [
                    (z & 0x00_0F) >>> (4 * 0),
                    (z & 0x00_F0) >>> (4 * 1),
                    (z & 0x0F_00) >>> (4 * 2),
                    (z & 0xF0_00) >>> (4 * 3),
                ]
                const payload = new Uint8Array([0x81, 0x01, 0x06, 0x02, v, w,
                    Y[3], Y[2], Y[1], Y[0],
                    Z[3], Z[2], Z[1], Z[0], 0xFF]);
                return await this.safeSend(payload);
            },
            PosInq: async (): Promise<{Y: number, Z: number}> => {
                const rsp = await this.safeInq(new Uint8Array([0x81, 0x09, 0x06, 0x12, 0xFF]));
                let Y: number = (
                    (rsp[3] << (4 * 0)) +
                    (rsp[2] << (4 * 1)) +
                    (rsp[1] << (4 * 2)) +
                    (rsp[0] << (4 * 3))
                )
                let Z: number = (
                    (rsp[7] << (4 * 0)) +
                    (rsp[6] << (4 * 1)) +
                    (rsp[5] << (4 * 2)) +
                    (rsp[4] << (4 * 3))
                )
                Y = inRange(Y, 0xDE00, 0xFFFF) ? Y - 0xFFFF : Y;
                Z = inRange(Z, 0x0400, 0xFFFF) ? Z - 0xFFFF : Z;
                return {Y, Z}
            }
        }
        return inter;
    }

    get Brightness() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Contrast() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get Setting() {
        const inter = {
            Save: async () => {
                await this.sendHex(`81 01 04 A5 10 FF`)
            },
        }
        return inter;
    }

    get AWB() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get ColorHue() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get OSD() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    private static TallyLightPayloads = {
        normal: new Uint8Array([0x81, 0x0A, 0x02, 0x02, 0x03, 0xFF]),
        flashing: new Uint8Array([0x81, 0x0A, 0x02, 0x02, 0x01, 0xFF]),
        on: new Uint8Array([0x81, 0x0A, 0x02, 0x02, 0x02, 0xFF]),
    };

    private state: 0 | 1 | 2 = 0;

    get TallyLight() {
        const inter = {
            Normal: async () => {
                await this.safeSend(PTZ.TallyLightPayloads.normal)
                this.state = 0;
            },
            Flashing: async () => {
                await this.safeSend(PTZ.TallyLightPayloads.flashing)
                this.state = 1;
            },
            On: async () => {
                await this.safeSend(PTZ.TallyLightPayloads.on)
                this.state = 2;
            },
            Set: async (state: 0 | 1 | 2) => {
                switch(state){
                    case 0:
                        await this.TallyLight.Normal();
                        break;
                    case 1:
                        await this.TallyLight.Flashing();
                        break;
                    case 2:
                        await this.TallyLight.On();
                        break;
                }
            },
            Get: () => {
                return this.state;
            },
        }
        return inter;
    }

    get NDI() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get MotionSync() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }

    get USBAudio() {
        const inter = {
            Stub: () => {
                console.warn("Stub");
            }
        }
        return inter;
    }
}