
// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, pluginFunction>;
let map: pluginMap;
export function load() {
    map = {};
    map["say"] = sayToConsole;
    return map;
}

function sayToConsole(text: string){
    console.log(text);
}