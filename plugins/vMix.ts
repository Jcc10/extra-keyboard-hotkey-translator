import { parse } from "https://deno.land/x/ts_xml_parser/mod.ts"
import { parse as newparse } from "https://deno.land/x/xml/mod.ts";

// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, pluginFunction>;
let map: pluginMap;
let mixers: Map<number, vMix>;
export function load() {
    mixers = new Map();
    map = {};
    map["new"] = ([host, port, id]: [string, number, number | undefined | null]): number => {
        const n = id ?? mixers.size;
        // deno-lint-ignore camelcase
        const new_vMix = new vMix(host, port)
        mixers.set(n, new_vMix);
        return n;
    };
    map["get"] = ([id]: [number]) => {
        return mixers.get(id);
    }
    return map;
}

export class vMix {
    readonly host:string;
    readonly port:number;
    readonly endpoint_base:string;
    constructor(host: string, port: number) {
        this.host = host;
        this.port = port;
        this.endpoint_base = `http://${this.host}:${this.port}/api/?`;
    }

    private static boolParse(s: string) {
        return s == "True" ? true : false;
    }

    private async sendRequest(f: string, opts?: Map<string, string>){
        let s = "";
        if(opts){
            for(const [opt, val] of opts.entries()){
                s += `&${opt}=${val}`
            }
        }
        const endpoint = `${this.endpoint_base}Function=${f}${s}`;
        const req = await fetch(endpoint);
        if(req.status >= 500){
            console.log("error 500 returned for request: " + endpoint)
        }
    }

    private async sendInfoRequest(){
        const endpoint = `${this.endpoint_base}`;
        const req = await fetch(endpoint);
        if(req.status >= 500){
            throw new Error("error 500 returned for request: " + endpoint)
        }
        const document = req.text();
        if(!document){
            throw new Error(`Invalid response.`);
        }
        return document;
    }

    public async getInfo() {
        const req = await this.sendInfoRequest();
        const parsed = parse(req);
        return parsed;
    }

    public async getInfo2() {
        const req = await this.sendInfoRequest();
        const parsed = newparse(req);
        return parsed;
    }

    FadeToBlack() {
        this.sendRequest("FadeToBlack");
    }

    Preview(to: string | number) {
        const opts = new Map<string, string>()
        opts.set("Input", to + "");
        this.sendRequest("PreviewInput", opts);
    }

    get Title() {
        const inter = {
            SetText: (input: string | number, location: "Headline" | "Description", text: string) => {  
                const opts = new Map<string, string>()
                opts.set("Input", input + "");
                opts.set("SelectedName", location + ".Text");
                opts.set("Value", text);
                this.sendRequest("SetText", opts);
            },
            In: (input: string | number) => {
                const opts = new Map<string, string>()
                opts.set("Input", input + "");
                this.sendRequest("OverlayInput1In", opts);
            },
            Out: (input: string | number) => {
                const opts = new Map<string, string>()
                opts.set("Input", input + "");
                this.sendRequest("OverlayInput1Out", opts);
            },
        }
        return inter;
    }

    get Transition() {
        const inter = {
        Cut: (to: string | number) => {
            const opts = new Map<string, string>()
            opts.set("Input", to + "");
            this.sendRequest("Cut", opts);
        },
        Fade: (to: string | number, duration: number) => {
            const opts = new Map<string, string>()
            opts.set("Input", to + "");
            opts.set("Duration", duration + "");
            this.sendRequest("Fade", opts);
        },
        CrossZoom: (to: string | number, duration: number) => {
            const opts = new Map<string, string>()
            opts.set("Input", to + "");
            opts.set("Duration", duration + "");
            this.sendRequest("CrossZoom", opts);
        },
        }
        return inter;
    }

    get Recording() {
        const inter = {
            start: () => {
                this.sendRequest("StartRecording")
            },
            stop: () => {
                this.sendRequest("StopRecording")
            },
            /*state: async () => {
                const fullState = await this.getInfo2();
                const state = vMix.boolParse(fullState.root.recording);
                return state;
            },*/
        };
        return inter;
    }

    get Streams() {
        const inter = {
            start: (n?: 0 | 1 | 2) => {
                if(n){
                    const opts = new Map<string, string>()
                    opts.set("Value", n + "");
                    this.sendRequest("StartStreaming", opts)
                } else {
                    this.sendRequest("StartStreaming")
                }
            },
            stop: (n?: 0 | 1 | 2) => {
                if(n){
                    const opts = new Map<string, string>()
                    opts.set("Value", n + "");
                    this.sendRequest("StopStreaming", opts)
                } else {
                    this.sendRequest("StopStreaming")
                }
            },
            key: (n: 0 | 1 | 2, key: string) => {
                const opts = new Map<string, string>()
                opts.set("Value", n + "," + key);
                this.sendRequest("StopStreaming", opts)
            },
            /*state: async () => {
                const fullState = await this.getInfo2();
                const state = vMix.boolParse(fullState.xml);
                return state;
            },*/
        };
        return inter;
    }
}