export function names(number: string): {name: string, title: string}{
    switch(number){
        case "0":
            return {name: "Dan Meyerpeter", title: "Lead Pastor"};
        case "1":
            return {name: "Jacob Johnson", title: "Youth Director"};
        case "2":
            return {name: "Natalie Godshall", title: "Executive Director"};
        case "3":
            return {name: "Matt Godshall", title: "Worship Leader"};
        case "4":
            return {name: "Bonnie Dighton", title: "Pastoral Intern"};
        case "5":
            return {name: "Bill Tanner", title: "Care Home Ministry"};
        case "6":
            return {name: "Jerry Oliaro", title: "Leadership Team Chair"};
        case "7":
            return {name: "Lori Dunn", title: "Tuesday Night Dinner"};
        case "8":
            return {name: "Gene Edwards", title: "Property and Facilities"};
        case "9":
            return {name: "Liam Gebhart", title: ""};
        default:
            return {name: "", title: ""};
    }
}