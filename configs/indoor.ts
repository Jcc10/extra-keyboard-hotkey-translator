export function recall(view: string, camera: string): {zoom: number, focus: number, af: boolean, X: number, Y: number, Z: number} {
    let ret: {zoom: number, focus: number, af: boolean,
        X: number, Y: number, Z: number} =
        {zoom: 0, focus: 0, af: true,
            X: 0, Y: 0, Z: 0};
    switch(view){
        case "test":
            if(camera == "C0"){
                ret = {...ret, zoom: 0, focus: 0, X: 0, Y: 0, Z: 0};
            }
            if(camera == "R0"){
                ret = {...ret, zoom: 0, focus: 0, X: 0, Y: 0, Z: 0};
            }
            break;
    }
    return ret;
}