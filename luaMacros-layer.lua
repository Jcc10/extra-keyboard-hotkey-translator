local pathWithFilename="C:\\Users\\RCC Broadcast\\LuaMacros\\";

--KB Verification
--lmc_assign_keyboard('MACROS');

local kbID =  'VID_03F0';
local filename = "C:\\Users\\RCC Broadcast\\Desktop\\macros\\pressed.txt"

lmc.minimizeToTray = true
lmc_minimize()
clear()
-- assign logical name to macro keyboard
lmc_device_set_name('MACROS', kbID);
lmc_print_devices()
local codes = {
      ["ctrl"]  = 17,
      ["alt"]   = 18,
      ["shift"] = 16,
      ["enter"] = 13,
      ["win"]   = 91,
      ["tab"]   = 9,
      ["space"] = 32,
      ["back"]  = 8,
      ["app"]   = 93,
      ["left"]  = 37,
      ["right"] = 39,
      ["up"]    = 38,
      ["down"]  = 40,
      ["insert"]= 45,
      ["delete"]= 46,
      ["home"]  = 36,
      ["end"]   = 35,
      ["pup"]   = 33,
      ["esc"]   = 27,
      ["plus"]  = 187,
      ["screen"]= 255,
};

sendToFile = function (key)
      --print('It was assigned string:    ' .. key)
      local file = io.open(filename, "w")
      file:write(key)
      file:flush() --"flush" means "save." Lol.
      file:close() -- Actually closes file
      --lmc_send_keys('{F24}')  -- This presses F24. Using the F24 key to trigger AutoHotKey is probably NOT the best method. Feel free to program something better!
end
                 
local keys = '';
keyDown = function (key, flag)
        local intKey = key
        if (
           key == 17 or -- ctrl
           key == 16 or -- shift
           key == 160 or -- shift
           key == 18 or -- alt
           key == 13 -- enter
           ) then
           intKey = key .. '|' .. flag
        end
        if not string.find(keys, intKey) then
           keys = keys .. ' ' .. intKey
        end
end

keyUp = function (key)
      keys = ''
end

-- define callback for whole device
lmc_set_handler('MACROS', function(button, direction, _ts, flags)
  if (direction == 1) then  -- ignore down
     keyDown(button, flags)
  elseif (direction == 0) then
     keyUp(button)
  end
  --print('k : ' .. keys)
  --print('Callback for device: button ' .. button .. ', direction '..direction..', ts '.._ts..', flags '..flags)
  sendToFile(keys)
end);


