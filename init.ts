// deno-lint-ignore no-explicit-any
type pluginFunction = (...args: [any])=>any;
type pluginMap = Record<string, Record<string, pluginFunction>>;

export const init = async (plugins: pluginMap) => {
    plugins.vMix.new(["127.0.0.1", 8088, 0]);
    plugins.ptz.new(["192.168.10.88", 0]);
    plugins.ptz.new(["192.168.10.89", 1]);
    const cameras = [
        plugins.ptz.get([0]).connect(),
        plugins.ptz.get([1]).connect()
    ];
    await Promise.all(cameras);
}